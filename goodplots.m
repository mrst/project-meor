close all

p1_1 = states{50}.meta;
p1_2 = states{150}.meta;
p1_3 = states{200}.meta;
p1_4 = states{350}.meta;
p2_1 = statesbio{50}.meta;
p2_2 = statesbio{150}.meta;
p2_3 = statesbio{200}.meta;
p2_4 = statesbio{350}.meta;
eta = .5;
para = @(m) eta.*((5.*m).^2 + 5.*m + 1);
%power = @(m) eta + 1.4019.*m.^.1653;
sW = [states{50}.s(:,1) states{150}.s(:,1) states{200}.s(:,1) states{350}.s(:,1)];
sO = [states{50}.s(:,2) states{150}.s(:,2) states{200}.s(:,2) states{350}.s(:,2)];
sWB = [statesbio{50}.s(:,1) statesbio{150}.s(:,1) statesbio{200}.s(:,1) statesbio{350}.s(:,1)];
sOB = [statesbio{50}.s(:,2) statesbio{150}.s(:,2) statesbio{200}.s(:,2) statesbio{350}.s(:,2)];

partition1 = 1.*(sW(:,1).*1000)./(sO(:,1).*800);
partition2 = 1.*(sW(:,2).*1000)./(sO(:,2).*800);
partition3 = 1.*(sW(:,3).*1000)./(sO(:,3).*800);
partition4 = 1.*(sW(:,4).*1000)./(sO(:,4).*800);
partition1B = 1.*(sWB(:,1).*1000)./(sOB(:,1).*800);
partition2B = 1.*(sWB(:,2).*1000)./(sOB(:,2).*800);
partition3B = 1.*(sWB(:,3).*1000)./(sOB(:,3).*800);
partition4B = 1.*(sWB(:,4).*1000)./(sOB(:,4).*800);

%meta_eff = meta.*partition./(partition + 1);
meta_eff = @(p,meta) meta.*p./(p+1);
surfa = [1*10^-4, 0.2, 1.5*10^4];
ift = @(s) 29.*(-tanh(surfa(3).*s-surfa(2))+1+surfa(1))./...
        (-tanh(-surfa(2))+1+surfa(1));

sigma1 = ift(double(meta_eff(partition1,p1_1)));
sigma2 = ift(double(meta_eff(partition2,p1_2)));
sigma3 = ift(double(meta_eff(partition3,p1_3)));
sigma4 = ift(double(meta_eff(partition4,p1_4)));
sigma1B = ift(double(meta_eff(partition1B,p2_1)));
sigma2B = ift(double(meta_eff(partition2B,p2_2)));
sigma3B = ift(double(meta_eff(partition3B,p2_3)));
sigma4B = ift(double(meta_eff(partition4B,p2_4)));

f1 = (sigma1/29).^(1/6);
f2 = (sigma2/29).^(1/6);
f3 = (sigma3/29).^(1/6);
f4 = (sigma4/29).^(1/6);
f1B = (sigma1B/29).^(1/6);
f2B = (sigma2B/29).^(1/6);
f3B = (sigma3B/29).^(1/6);
f4B = (sigma4B/29).^(1/6);

sor1 = f1.*.4;
sor2 = f2.*.4;
sor3 = f3.*.4;
sor4 = f4.*.4;
sor1B = f1B.*.4;
sor2B = f2B.*.4;
sor3B = f3B.*.4;
sor4B = f4B.*.4;

figure
subplot(221)
plot(sor1,'LineWidth',2)
hold on
plot(sor1B, 'r', 'LineWidth', 2)
ylabel('S_{or}')
title('50 Days')
subplot(222)
plot(sor2,'LineWidth',2)
hold on
plot(sor2B, 'r', 'LineWidth', 2)
ylabel('S_{or}')
title('350 Days')
subplot(223)
plot(sor3,'LineWidth',2)
hold on
plot(sor3B, 'r', 'LineWidth', 2)
ylabel('S_{or}')
title('500 Days')
subplot(224)
plot(sor4,'LineWidth',2)
hold on
plot(sor4B, 'r', 'LineWidth', 2)
ylabel('S_{or}')
title('700 Days')

figure
subplot(221)
plot(para(p2_1),'g','LineWidth', 2)
hold on
%plot(power(p1_1),'LineWidth', 2)
plot(para(p1_1),'b','LineWidth', 2)
ylabel('\eta_w')
title('50 Days')
subplot(222)
plot(para(p2_2),'g','LineWidth', 2)
hold on
plot(para(p1_2),'b','LineWidth', 2)
%plot(power(p1_2),'LineWidth', 2)
ylabel('\eta_w')
title('350 Days')
subplot(223)
plot(para(p2_3),'g','LineWidth', 2)
hold on
plot(para(p1_3),'b','LineWidth', 2)
%plot(power(p1_3),'LineWidth', 2)
ylabel('\eta_w')
title('500 Days')
subplot(224)
plot(para(p2_4),'g','LineWidth', 2)
hold on
plot(para(p1_4),'b','LineWidth', 2)
%plot(power(p1_4),'LineWidth', 2)
ylabel('\eta_w')
title('700 Days')


figure
plot(states{300}.meta,'k', 'LineWidth', 2)
hold on
plot(states{300}.n, 'LineWidth', 2)
plot(states{300}.m, 'r', 'LineWidth', 2)
title('300 Days')
legend('Biopolymer', 'Nutrients', 'Bacteria')
ylabel('Concentration kg/m^3')
xlabel('Length m')
figure
plot(statesbio{300}.meta,'k', 'LineWidth', 2)
hold on
plot(statesbio{300}.n, 'LineWidth', 2)
plot(statesbio{300}.m + statesbio{300}.bio, 'r', 'LineWidth', 2)
plot(statesbio{300}.bio, 'g', 'LineWidth', 2)
title('300 Days')
legend('Biosurfactant', 'Nutrients', 'Total Bacteria', 'Biofilm')
ylabel('Concentration kg/m^3')
xlabel('Length m')

% figure
% subplot(221)
% plot(states{50}.meta,'k', 'LineWidth', 2)
% hold on
% plot(states{50}.m, 'r', 'LineWidth', 2)
% plot(states{50}.n, 'b', 'LineWidth', 2)
% ylabel('Concentration kg/m^3')
% title('50 Days')
% subplot(222)
% plot(states{250}.meta, 'k','LineWidth', 2)
% hold on
% plot(states{250}.m, 'r', 'LineWidth', 2)
% plot(states{250}.n, 'b', 'LineWidth', 2)
% ylabel('Concentration kg/m^3')
% title('250 Days')
% subplot(223)
% plot(states{350}.meta, 'k','LineWidth', 2)
% hold on
% plot(states{350}.m, 'r', 'LineWidth', 2)
% plot(states{350}.n, 'b', 'LineWidth', 2)
% ylabel('Concentration kg/m^3')
% title('350 Days')
% subplot(224)
% plot(states{450}.meta, 'k','LineWidth', 2)
% hold on
% plot(states{450}.m, 'r', 'LineWidth', 2)
% plot(states{450}.n, 'b', 'LineWidth', 2)
% ylabel('Concentration kg/m^3')
% title('450 Days')



figure
subplot(221)
plot(states{50}.meta,'k:', 'LineWidth', 2)
hold on
plot(statesbio{50}.meta,'k', 'LineWidth', 2)
ylabel('M_s')
title('50 Days')
subplot(222)
plot(states{350}.meta, 'k:','LineWidth', 2)
hold on
plot(statesbio{350}.meta,'k', 'LineWidth', 2)
ylabel('M_s')
title('350 Days')
subplot(223)
plot(states{500}.meta, 'k:','LineWidth', 2)
hold on
plot(statesbio{500}.meta,'k', 'LineWidth', 2)
ylabel('M_s')
title('500 Days')
subplot(224)
plot(states{700}.meta, 'k:','LineWidth', 2)
hold on
plot(statesbio{700}.meta,'k', 'LineWidth', 2)
ylabel('M_s')
title('700 Days')

figure
subplot(221)
plot(states{50}.m,'r:', 'LineWidth', 2)
hold on
plot(statesbio{50}.m+statesbio{50}.bio,'r', 'LineWidth', 2)
plot(statesbio{50}.bio,'g', 'LineWidth', 2)
ylabel('B')
title('50 Days')
subplot(222)
plot(states{350}.m,'r:', 'LineWidth', 2)
hold on
plot(statesbio{350}.m+statesbio{350}.bio,'r', 'LineWidth', 2)
plot(statesbio{350}.bio,'g', 'LineWidth', 2)
ylabel('B')
title('350 Days')
subplot(223)
plot(states{700}.m, 'r:','LineWidth', 2)
hold on
plot(statesbio{700}.m+statesbio{700}.bio,'r', 'LineWidth', 2)
plot(statesbio{700}.bio,'g', 'LineWidth', 2)
ylabel('B')
title('700 Days')
subplot(224)
plot(states{1000}.m, 'r:','LineWidth', 2)
hold on
plot(statesbio{1000}.m+statesbio{1000}.bio,'r', 'LineWidth', 2)
plot(statesbio{1000}.bio,'g', 'LineWidth', 2)
ylabel('B')
title('1000 Days')
% 
% figure
% subplot(221)
% plot(states{1}.pressure,'r', 'LineWidth', 2)
% hold on
% plot(states2{1}.pressure,'k', 'LineWidth', 2)
% ylabel('Pressure Pa')
% title('1 Day')
% subplot(222)
% plot(states{20}.pressure,'r', 'LineWidth', 2)
% hold on
% plot(states2{20}.pressure,'k', 'LineWidth', 2)
% ylabel('Pressure Pa')
% title('20 Days')
% subplot(223)
% plot(states{300}.pressure, 'r','LineWidth', 2)
% hold on
% plot(states2{300}.pressure,'k', 'LineWidth', 2)
% ylabel('Pressure Pa')
% title('300 Days')
% subplot(224)
% plot(states{500}.pressure, 'r','LineWidth', 2)
% hold on
% plot(states2{500}.pressure,'k', 'LineWidth', 2)
% ylabel('Pressure Pa')
% title('500 Days')

figure
subplot(221)
plot(states{50}.n,':', 'LineWidth', 2)
hold on
plot(statesbio{50}.n,  'LineWidth', 2)
ylabel('N')
title('50 Days')
subplot(222)
plot(states{350}.n,':', 'LineWidth', 2)
hold on
plot(statesbio{350}.n,  'LineWidth', 2)
ylabel('N')
title('350 Days')
subplot(223)
plot(states{700}.n, ':','LineWidth', 2)
hold on
plot(statesbio{700}.n,  'LineWidth', 2)
ylabel('N')
title('700 Days')
subplot(224)
plot(states{1000}.n, ':','LineWidth', 2)
hold on
plot(statesbio{1000}.n,  'LineWidth', 2)
ylabel('N')
title('1000 Days')

figure
subplot(221)
plot(states{50}.s(:,1), 'LineWidth', 2)
hold on
plot(statesbio{50}.s(:,1),'r', 'LineWidth', 2)
plot(statesow{50}.s(:,1),'k:', 'LineWidth', 2)
ylabel('S_w')
title('50 Days')
xlim([0,400])
ylim([0 1])
subplot(222)
plot(states{150}.s(:,1), 'LineWidth', 2)
hold on
plot(statesbio{150}.s(:,1),'r', 'LineWidth', 2)
plot(statesow{150}.s(:,1),'k:', 'LineWidth', 2)
ylabel('S_w')
title('150 Days')
xlim([0,400])
ylim([0 1])
subplot(223)
plot(states{300}.s(:,1),'LineWidth',2)
hold on
plot(statesbio{300}.s(:,1),'r','LineWidth',2)
plot(statesow{300}.s(:,1),'k:', 'LineWidth', 2)
ylabel('S_w')
xlim([0,400])
ylim([0 1])
title('300 Days')
subplot(224)
plot(states{450}.s(:,1),'LineWidth',2)
hold on
plot(statesbio{450}.s(:,1),'r','LineWidth',2)
plot(statesow{450}.s(:,1),'k:', 'LineWidth', 2)
ylabel('S_w')
title('450 Days')
xlim([0,400])
ylim([0 1])