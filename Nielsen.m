%clear all
tic
nx = 400;
[ny nz] = deal(1);
G = cartGrid([nx ny nz], [400 100 100]);
G = computeGeometry(G);
rock.poro = ones(G.cells.num, 1).*.4;
rock.perm = ones(G.cells.num, 1).*100*milli*darcy;


deck = readEclipseDeck('MICROBE.DATA');
deck = convertDeckUnits(deck);
fluid = initDeckADIFluid(deck);

gravity off

state0 = initResSol(G, 100*barsa, [.3 .7]);
state0.m = zeros(G.cells.num, 1);
state0.n = zeros(G.cells.num, 1);
state0.meta = zeros(G.cells.num, 1);
state0.bio = zeros(G.cells.num, 1);

model = MEORaModel(G, rock, fluid);
modelow = TwoPhaseOilWaterModel(G, rock, fluid);
model.biosurf = true;

model.yield_metabolite = .18;
model.yield_microbe = .82;
model.halfsat_microbe = 1;
model.halfsat_metabolite = 1;
model.growth_max_microbe = .2/day;
model.growth_max_metabolite = .2/day;

model.stateplots = false;
if model.stateplots
      for i = 1 : 5
         figure(i)
      end
end


ijk = gridLogicalIndices(G);
srcCell = find(ijk{1}==1 & ijk{2}==1 & ijk{3}==1);
snkCell = find(ijk{1}==40 & ijk{2}==1 & ijk{3}==1);
srcVal = .000002.*ones(numel(srcCell),1);
src = addSource([], srcCell, srcVal, 'sat', [1 0]);
src = addSource(src, snkCell, -srcVal, 'sat', [0 0]);
src.m = .005.*ones(size(src.sat,1), 1);
src.n = .01.*ones(size(src.sat,1), 1);

bc = fluxside([],G, 'LEFT', 800.*meter^3/day(), 'sat', [1 0]);
%bc = fluxside(bc, G, 'RIGHT', -800.*meter^3/day(), 'sat', [0 0]);
bc = pside(bc, G, 'RIGHT', 100*barsa, 'sat', [0 0]);
bc.m = .005.*ones(size(src.sat,1), 1);
bc.n = .01.*ones(size(src.sat,1), 1);
bc.meta = 0.*ones(size(src.sat,1), 1);

dt = 1*day;
%380 is .19pv
nt = 2000/(dt/day);
schedule.step.val = dt.*ones(nt,1);
schedule.step.control = ones(nt,1);
W = addWell([], G, rock, srcCell, 'type', 'rate', 'val', 800/day, 'comp_i', [1 0]);
W = addWell(W, G, rock, snkCell, 'type', 'bhp', 'val', 100*barsa, 'comp_i', [0 0]);
schedule.control(1).W = [];
schedule.control(1).bc = bc;
schedule.control(1).src = [];

modelbio = model;
modelbio.biofilm = true;
modelbio.langmuir = [.001 .0017];

[~, statesow] = simulateScheduleAD(state0, modelow, schedule);
[~, states] = simulateScheduleAD(state0, model, schedule);
%[~, statesbio] = simulateScheduleAD(state0, modelbio, schedule);



%        figure;
% plotToolbar(G, states);
% view(2);
% axis tight;
% colorbar;
%%
oilw = zeros(length(states)+1, 1);
oilw(1) = 0;
oil = oilw;
oilbio = oil;
for i = 1:length(states)
    oilw(i+1) = oilw(i) + statesow{i}.flux(400,2);
    oil(i+1) = oil(i) + states{i}.flux(400,2);    
    %oilbio(i+1) = oilbio(i) + statesbio{i}.flux(400,2);    
end
figure
plot(oilw.*day./(400*100*100*.4*.7),'k:','LineWidth', 2)
hold on
plot(oil.*day./(400*100*100*.4*.7),'LineWidth', 2)
plot(oilbio.*day./(400*100*100*.4*.7),'r','LineWidth', 2)
disp(oilw(end).*day./(400*100*100*.4*.7))
disp(oil(end).*day./(400*100*100*.4*.7))
disp(oilbio(end).*day./(400*100*100*.4*.7))
disp((oil(end)-oilw(end))./oilw(end))
disp((oilbio(end)-oil(end))./oil(end))
xlim([0 length(oilw)])
ylim([0,1])
legend('Water', 'MEOR','Biofilm')
xlabel('Days')
ylabel('Recovery')
toc