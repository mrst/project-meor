close all

days = [350];
number = days./2.5;
for i = 1:length(days)
    hFig = figure();
    set(hFig, 'Position', [400 400 700 170])
    data = states_ow{number(i),1}.s(:,1);
    data(data==0) = NaN;
    plotCellData(G,data,'EdgeColor', 'none')
    axis equal tight
    xlim([0 400])
    colorbar
    caxis([0 1])
    title('Water')
end