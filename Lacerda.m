%clear all
tic
nx = 400;
[ny nz] = deal(1);
G = cartGrid([nx ny nz], [400 100 100]);
G = computeGeometry(G);
rock.poro = ones(G.cells.num, 1).*.4;
rock.perm = ones(G.cells.num, 1).*100*milli*darcy;

%%
deck = readEclipseDeck('POLYMER.DATA');
deck = convertDeckUnits(deck);
fluid = initDeckADIFluid(deck);

gravity off

state0 = initResSol(G, 100*barsa, [.16 .84]);
state0.m = zeros(G.cells.num, 1);
state0.n = zeros(G.cells.num, 1);
state0.meta = zeros(G.cells.num, 1);
state0.bio = zeros(G.cells.num, 1);

model = MEORaModel(G, rock, fluid);
modelow = TwoPhaseOilWaterModel(G, rock, fluid);
model.biopoly = true;
model.growth_max_microbe = .01/day;
model.growth_max_metabolite = .01/day;

model.stateplots = false;
if model.stateplots
      for i = 1 : 5
         figure(i)
      end
end


ijk = gridLogicalIndices(G);
srcCell = find(ijk{1}==1 & ijk{2}==1 & ijk{3}==1);
snkCell = find(ijk{1}==40 & ijk{2}==1 & ijk{3}==1);
srcVal = .000002.*ones(numel(srcCell),1);
src = addSource([], srcCell, srcVal, 'sat', [1 0]);
src = addSource(src, snkCell, -srcVal, 'sat', [0 0]);
src.m = 1.*ones(size(src.sat,1), 1);
src.n = 10.*ones(size(src.sat,1), 1);

bc = fluxside([],G, 'LEFT', 800.*meter^3/day(), 'sat', [1 0]);
%bc = fluxside(bc, G, 'RIGHT', -800.*meter^3/day(), 'sat', [0 0]);
bc = pside(bc, G, 'RIGHT', 100*barsa, 'sat', [0 0]);
%bc.m = .005.*ones(size(src.sat,1), 1);
%bc.n = .01.*ones(size(src.sat,1), 1);
bc.m = [.2, 0];
bc.n = [1, 0];
bc.meta = 0.*ones(size(src.sat,1), 1);
dt = 1*day;
nt = 2000;
schedule.step.val = dt.*ones(nt,1);
schedule.step.control = ones(nt,1);
%W = addWell([], G, rock, srcCell, 'rate', 800/day);
%W = addWell(W, G, rock, snkCell, 'bhp', -1);
schedule.control(1).W = [];
schedule.control(1).bc = bc;
schedule.control(1).src = [];

modelBio = model;
modelBio.biofilm = true;
modelBio.langmuir = [0.001, 0.0017];

%[~, states] = simulateScheduleAD(state0, model, schedule);
[~, statesow] = simulateScheduleAD(state0, modelow, schedule);
%[~, statesbio] = simulateScheduleAD(state0, modelBio, schedule);
%%
%        figure;
% plotToolbar(G, states);
% view(2);
% axis tight;
% colorbar;
oilw = zeros(length(states)+1, 1);
oilw(1) = 0;
oil = oilw;
oilbio = oil;
for i = 1:length(states)
    oilw(i+1) = oilw(i) + statesow{i}.flux(400,2);
    oil(i+1) = oil(i) + states{i}.flux(400,2);    
    oilbio(i+1) = oilbio(i) + statesbio{i}.flux(400,2);    
end
figure
plot(oilw.*day./(400*100*100*.4*.84))
hold on
plot(oil.*day./(400*100*100*.4*.84))
plot(oilbio.*day./(400*100*100*.4*.84),'g')
disp(oilw(end).*day./(400*100*100*.4*.84))
disp(oil(end).*day./(400*100*100*.4*.84))
disp(oilbio(end).*day./(400*100*100*.4*.84))
disp((oil(end)-oilw(end))./oilw(end))
disp((oilbio(end)-oil(end))./oil(end))
toc