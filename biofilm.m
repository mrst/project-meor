clear all
tic
close all
nx = 80;
ny = 20;
nz = 10;
G = cartGrid([nx ny nz], [400 100 100]);
G = computeGeometry(G);
ijk = gridLogicalIndices(G);
rock.poro = ones(G.cells.num, 1).*.3;
rock.perm = ones(G.cells.num, 1).*100*milli*darcy;
rock1 = rock;
thief = [];
for x = 1:nx
    for y = ny/2-3:ny/2+4
        for z = 4:7
           thief = [thief; find(ijk{1}==x  & ijk{2}==y  & ijk{3}==z)];
        end
    end
end
rock.poro(thief) = .35;
rock.perm(thief) = 300*milli*darcy;
%%

deck = readEclipseDeck('POLYMER.DATA');
deck = convertDeckUnits(deck);
fluid = initDeckADIFluid(deck);

gravity on

ooip = .84;
state0 = initResSol(G, 100*barsa, [1-ooip ooip]);
state0.m = zeros(G.cells.num, 1);
state0.n = zeros(G.cells.num, 1);
state0.meta = zeros(G.cells.num, 1);
state0.bio = zeros(G.cells.num, 1);

model = MEORaModel(G, rock, fluid);
model.biofilm = true;
model.biosurf = true;
%model.biopoly = true;

model.yield_metabolite = .5;
model.yield_microbe = .5;
model.halfsat_microbe = 1;
model.halfsat_metabolite = 1;
model.growth_max_microbe = .2/day;
model.growth_max_metabolite = .2/day;
model.langmuir = [.001 .0017];

model.stateplots = false;
if model.stateplots
      for i = 1 : 6
         figure(i)
      end
end



srcCell = find(ijk{1}==1 & ijk{2}==1 & ijk{3}==1);
snkCell = find(ijk{1}==40 & ijk{2}==1 & ijk{3}==1);
%srcVal = .000002.*ones(numel(srcCell),1);
%src = addSource([], srcCell, srcVal, 'sat', [1 0]);
%src = addSource(src, snkCell, -srcVal, 'sat', [0 0]);
%src.m = .005.*ones(size(src.sat,1), 1);
%src.n = .01.*ones(size(src.sat,1), 1);

bc = fluxside([],G, 'LEFT', 800.*meter^3/day(), 'sat', [1 0]);
%bc = fluxside(bc, G, 'RIGHT', -800.*meter^3/day(), 'sat', [0 0]);
bc = pside(bc, G, 'RIGHT', 100*barsa, 'sat', [0 0]);
bc.m = [.2.*ones(size(bc.sat,1)./2, 1); 0.*ones(size(bc.sat,1)./2, 1)];
bc.n = [1.*ones(size(bc.sat,1)./2, 1); 0.*ones(size(bc.sat,1)./2, 1)] ;
bc.meta = [0.*ones(size(bc.sat,1)./2, 1); 0.*ones(size(bc.sat,1)./2, 1)];

dt = 2.5*day;
%380 is .19pv
nt = 50/(dt/day);
schedule.step.val = dt.*ones(nt,1);
schedule.step.control = ones(nt,1);
W = addWell([], G, rock, srcCell, 'type', 'rate', 'val', 800/day, 'comp_i', [1 0]);
W = addWell(W, G, rock, snkCell, 'type', 'bhp', 'val', 100*barsa, 'comp_i', [0 0]);
schedule.control(1).W = [];
schedule.control(1).bc = bc;
schedule.control(1).src = [];

modelow = TwoPhaseOilWaterModel(G, rock, fluid);
modelp = model;
modelp.biosurf = false;
modelp.biopoly = true;
%%
[~, states_ow] = simulateScheduleAD(state0, modelow, schedule);
[~, states_surf] = simulateScheduleAD(state0, model, schedule);
[~, states_poly] = simulateScheduleAD(state0, modelp, schedule);


%        figure;
% plotToolbar(G, states);
% view(2);
% axis tight;
% colorbar;
%%
production = [];
for y = 1:ny
    for z = 1:nz
       production = [production; find(ijk{1}==nx  & ijk{2}==y  & ijk{3}==z)];
    end
end
oilw = zeros(length(states_poly)+1, 1);
oils = oilw;
oilp = oils;
for i = 1:length(states_poly)
    oilw(i+1) = oilw(i) + sum(states_ow{i}.flux(production,2)).*dt;
    oils(i+1) = oils(i) + sum(states_surf{i}.flux(production,2)).*dt;    
    oilp(i+1) = oilp(i) + sum(states_poly{i}.flux(production,2)).*dt;    
end
figure
plot(oilw./(sum(model.operators.pv).*ooip),'k:', 'LineWidth', 2)
hold on
plot(oils./(sum(model.operators.pv).*ooip), 'LineWidth', 2)
plot(oilp./(sum(model.operators.pv).*ooip), 'r', 'LineWidth', 2)
legend('Water', 'Surfactant', 'Polymer','Location','Best')
disp(oilw(end)./(sum(model.operators.pv).*ooip))
disp(oils(end)./(sum(model.operators.pv).*ooip))
disp(oilp(end)./(sum(model.operators.pv).*ooip))
disp((oils(end)-oilw(end))./oilw(end))
disp((oilp(end)-oilw(end))./oilw(end))

%%
figure
plotToolbar(G,states_surf)
%colorbar
axis tight
figure
plotToolbar(G,states_poly)
%colorbar
axis tight
figure
plotToolbar(G,states_ow)
%colorbar
axis tight
toc