# You have to install a version of MRST based on the bitbucket repository
# (https://bitbucket.org/account/user/mrst/projects/MRST)
# There, you will checkout the branch meor
# In practice it means following the following steps:

# Create directory where you install an mrst
mkdir mrst-bitbucket-dir


# Clone the following mrst repos and checkout the branch meor in each of them

git clone git@bitbucket.org:mrst/mrst-autodiff.git
cd mrst-bitbucket-dir/mrst-autodiff
git checkout -b meor origin/meor

cd mrst-bitbucket-dir
git clone git@bitbucket.org:mrst/mrst-core.git
cd mrst-bitbucket-dir/mrst-core
git checkout -b meor origin/meor

cd mrst-bitbucket-dir
git clone git@bitbucket.org:mrst/mrst-model-io.git
cd mrst-bitbucket-dir/mrst-model-io
git checkout -b meor origin/meor

cd mrst-bitbucket-dir
git clone git@bitbucket.org:mrst/mrst-solvers.git
cd mrst-bitbucket-dir/mrst-solvers
git checkout -b meor origin/meor

cd mrst-bitbucket-dir
git clone git@bitbucket.org:mrst/mrst-visualization.git
cd mrst-bitbucket-dir/mrst-visualization
git checkout -b meor origin/meor

# In Matlab, from a clean start, run the following command to set up the path
# correctly to the mrst-bitbucket version
run 'mrst-bitbucket-dir/mrst-core/startup'

# Then, you are ready to run the scripts for MEOR
# In Matlab:
MEORtest
Xtest
