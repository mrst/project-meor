clear all
close all

nx = 10;
ny = 10;
nz = 5;
gt = makeModel3([nx ny nz]);
gb = processGRDECL(gt);
G = computeGeometry(processGRDECL(makeModel3([nx ny nz])));
G = computeGeometry(cartGrid([nx ny nz], [100 100 15]));
%G = computeGeometry(processGRDECL(simpleGrdecl([50 30 5],[], 'physDims', [500 300 15])));
ijk = gridLogicalIndices(G);
layers = 25:29;
rock.poro = [];
rock.perm = [];
for i = 1:nz
    rock_t = SPE10_rock(layers(i));
    rock_t.perm = convertFrom(rock_t.perm, milli*darcy);
    rock.poro = [rock.poro; rock_t.poro(1:(nx*ny))];
    rock.perm = [rock.perm; rock_t.perm(1:(nx*ny),:)];
end
rock.perm = convertFrom(rock.perm, milli*darcy);
is_pos = rock.poro>0;
rock.poro(~is_pos) = min(rock.poro(is_pos));
plotCellData(G,rock.poro)

deck = readEclipseDeck('test.data');
deck = convertDeckUnits(deck);
fluid = initDeckADIFluid(deck);
state0 = initResSol(G, 100*barsa, [1-.84, .84]);
modelow = TwoPhaseOilWaterModel(G, rock, fluid);
%%
bc = [];
src = [];
%src = addSource([], find(ijk{1}==nx/2 & ijk{2}==(ny/2) & ijk{3}==3),...
%                800*meter^3/day, 'sat', [1 0]);
%src = addSource(src, find(ijk{1}==nx & ijk{2}==ny & ijk{3}==3),...
                %-1, 'sat', [0 1]);
% bc = fluxside([], G, 'TOP', 500, nx/2-1:nx/2+1, ny/2-1:ny/2+1, 'sat', [1 0]);
% bc = pside(bc, G, 'WEST', 200*barsa, 1:3,1:5, 'sat', [0 0]);
% bc = pside(bc, G, 'WEST', 200*barsa, ny-3:ny,1:5, 'sat', [0 0]);
% bc = pside(bc, G, 'EAST', 200*barsa, 1:3,1:5, 'sat', [0 0]);
% bc = pside(bc, G, 'EAST', 200*barsa, ny-3:ny,1:5, 'sat', [0 0]);

bc = fluxside([], G, 'WEST', 50,5,5, 'sat', [1 0]);
%bc = pside(bc, G, 'EAST', 100*barsa, 5,5,'sat', [0 0]);

dt = 1*day;
nt = 20/(dt/day);
schedule.step.val = dt.*ones(nt,1);
schedule.step.control = ones(nt,1);
schedule.control(1).src = src;
schedule.control(1).W = [];
schedule.control(1).bc = bc;

[~, states] = simulateScheduleAD(state0, modelow, schedule);