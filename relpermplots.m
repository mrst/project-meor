clear all
close all

swi = .16;
sor = .23;
kro = @(s,sor,swi) .7.*((1-s-sor)/(1-swi-sor)).^2;
krw = @(s,sor,swi) .3.*((s-swi)/(1-swi-sor)).^2;

ift0 = 50;
ift1 = .05;
f = (ift1/ift0).^(1/7);
swi1 = f.*swi;
sor1 = f.*sor;

kw0 = krw(swi:.01:(1-sor),sor,swi);
ko0 = kro(swi:.01:(1-sor),sor,swi);
kw1 = krw(swi1:.01:(1-sor1),sor1,swi1);
ko1 = kro(swi1:.01:(1-sor1),sor1,swi1);
kw1m = linspace(kw1(1),kw1(end),length(kw1));
ko1m = linspace(ko1(1),ko1(end),length(ko1));

plot(swi:.01:(1-sor),kw0)
hold on
plot(swi1:.01:(1-sor1),kw1,'r')
plot(linspace(swi1,1-sor1,length(kw1m)),kw1m,'r')
plot(swi:.01:(1-sor),ko0,'k')
plot(swi1:.01:(1-sor1),ko1,'r')
plot(linspace(swi1,1-sor1,length(ko1m)),ko1m,'r')

kw = f.*kw1 +(1-f).*kw1m;
ko = f.*ko1 +(1-f).*ko1m;

figure;
plot(swi:.01:(1-sor),kw0)
hold on
plot(linspace(swi1,1-sor1,length(kw)),kw,'r')
plot(swi:.01:(1-sor),ko0,'k')
plot(linspace(swi1,1-sor1,length(ko)),ko,'r')

ow = f.*.7+1-f;
wo = f.*.3+1-f;
n = f.*2+1-f;
kro = @(s,sor,swi) ow.*((1-s-sor)/(1-swi-sor)).^n;
krw = @(s,sor,swi) wo.*((s-swi)/(1-swi-sor)).^n;
co = kro(swi1:.01:(1-sor1),sor1,swi1);
cw = krw(swi1:.01:(1-sor1),sor1,swi1);
figure
plot(swi:.01:(1-sor),kw0)
hold on
plot(linspace(swi1,1-sor1,length(cw)),cw,'r')
plot(swi:.01:(1-sor),ko0,'k')
plot(linspace(swi1,1-sor1,length(cw)),co,'r')
