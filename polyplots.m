%close all
figure

eta = .5; %centipose
m = logspace(-4,0,1000);
d = logspace(-4,0,100);


G = 100;

linear = eta + G.*m;
para = eta.*((10.*m).^2 + 10.*m + 1);
power = eta + 1.4019.*m.^.1653;
%comp = eta.*(.8144.*m.^5 - 8.7354.*m.^4 +29.4579.*m.^3 - 29.1649.*m.^2 +11.9176.*m+1.0263);

%semilogx(m,linear,'g')
hold on
loglog(m,para,'r','LineWidth', 2)
loglog(m,power,'LineWidth', 2)
loglog(d,eta.*ones(length(d)), ':k', 'LineWidth', 2)
%semilogx(m,comp,'k')
legend('Parabolic', 'Power', '\eta_0')
xlabel('Concentration of Biopolymer  kg/m^3')
ylabel('Water Viscosity cP')