%% Read case from file
% This example contains a simple $31\times31\times3$ fine grid containing
% two injectors in opposite corners and one producer in the middle of the
% domain. All wells are completed in the top layers of cells.
%
% The schedule being used contains first a period of injection with
% polymer, followed by a water flooding phase without polymer. Finally, the
% water rate is reduced for the final time steps.
%
clear all
try
   require add ad-core ad-blackoil ad-fi deckformat
catch
   mrstModule add ad-core ad-blackoil ad-fi deckformat
end
%%
%current_dir = fileparts(mfilename('fullpath'));
%fn    = fullfile(current_dir, 'POLYMER.DATA');

deck = readEclipseDeck('POLYMER.DATA');
deck = convertDeckUnits(deck);

G = initEclipseGrid(deck);
G = computeGeometry(G);

rock  = initEclipseRock(deck);
rock  = compressRock(rock, G.cells.indexMap);

fluid = initDeckADIFluid(deck);

% Oil rel-perm from 2p OW system.
% Needed by equation implementation function 'eqsfiOWExplictWells'.
fluid.krO = fluid.krOW;

gravity on

%% Set up simulation parameters
% We want a layer of oil on top of the reservoir and water on the bottom.
% To do this, we alter the initial state based on the logical height of
% each cell. The resulting oil concentration is then plotted.

ijk = gridLogicalIndices(G);

state0 = initResSol(G, 300*barsa, [ .9, .1]);
state0.s(ijk{3} == 1, 2) = .9;
state0.s(ijk{3} == 2, 2) = .8;

% Enforce s_w + s_o = 1;
state0.s(:,1) = 1 - state0.s(:,2);

% Add zero microbe/nutrient concentration to the state.
state0.m    = zeros(G.cells.num, 1);
state0.n = zeros(G.cells.num, 1);
state0.meta = zeros(G.cells.num,1);
s = setupSimComp(G, rock, 'deck', deck);

%% Set up systems.
% To quantify the effect of adding the polymer to the injected water, we
% will solve the same system both with and without polymer. This is done by
% creating both a Oil/Water/Polymer system and a Oil/Water system. Note
% that since the data file already contains polymer as an active phase we
% do not need to pass initADISystem anything other than the deck.

modelMEOR = MEORaModel(G, rock, fluid, 'inputdata', deck);
modelOW = TwoPhaseOilWaterModel(G, rock, fluid, 'inputdata', deck);

% Convert the deck schedule into a MRST schedule by parsing the wells
schedule = convertDeckScheduleToMRST(G, modelMEOR, rock, deck);


%% Run the schedule
% Once a system has been created it is trivial to run the schedule. Any
% options such as maximum non-linear iterations and tolerance can be set in
% the system struct.

[wellSolsMEOR, statesMEOR] = ...
   simulateScheduleAD(state0, modelMEOR, schedule);

%[wellSolsOW, statesOW] = simulateScheduleAD(state0, modelOW, schedule);

figure;
plotToolbar(G,statesMEOR);
view([-10, 14]);
axis tight;
colorbar;
