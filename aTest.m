%% POLYMER BOUNDARY CONDITIONS AND SOURCE EXAMPLE
%
% This example is made just to illustrate how one can setup a polymer
% simulation with boundary conditions and/or source.
% 

% Required modules
clear all
%mrstModule add deckformat ad-core ad-blackoil ad-fi mrst-gui


%% Setup case

% Grid, rock and fluid
deck  = readEclipseDeck('POLYMER.DATA');
deck  = convertDeckUnits(deck);
%G     = initEclipseGrid(deck);
%G     = computeGeometry(G);
%rock  = initEclipseRock(deck);
%rock  = compressRock(rock, G.cells.indexMap);
fluid = initDeckADIFluid(deck);
[nx, ny] = deal(32);
G = cartGrid([nx, 1], [500, 500]);
G = computeGeometry(G);
rock.perm = ones(G.cells.num,1)*100*milli*darcy;
rock.poro = ones(G.cells.num,1)*.3;

% Gravity
gravity on

% Initial state
state0      = initResSol(G, 100*barsa, [ .2, .8]);
%state0.c    = zeros(G.cells.num,1);
%state0.cmax = zeros(G.cells.num,1);
state0.m = zeros(G.cells.num,1);
state0.m_1 = zeros(G.cells.num,1);
state0.n = zeros(G.cells.num,1);
state0.meta = zeros(G.cells.num,1);


% Create model
model = MEORaModel(G, rock, fluid);
model.biosurf = true;
% Setup some schedule
dt = 5*day;
nt = 10;
clear schedule
schedule.step.val       = dt.*ones(nt,1);
schedule.step.control   = ones(nt,1);
schedule.control(1).W   = [];
schedule.control(1).bc  = [];
schedule.control(1).src = [];




%% Source

% Create source
ijk = gridLogicalIndices(G);
%srcCells = find(ijk{1}==5  & ijk{2}==5  & ijk{3}==2);
%snkCells = find(ijk{1}==26 & ijk{2}==26 & ijk{3}==2);
srcCells = find(ijk{1}==1  & ijk{2}==1);
snkCells = find(ijk{1}==32 & ijk{2}==1);
srcVals  = 0.001.*ones(numel(srcCells),1);
src = addSource( [], srcCells,  srcVals, 'sat', [1 0]);
src = addSource(src, snkCells, -srcVals, 'sat', [0 0]);
src.m = .1.*ones(size(src.sat,1), 1);
src.n = .1.*ones(size(src.sat,1), 1);
schedule.control(1).bc  = [];
schedule.control(1).src = src;

% Simulate
[~, states] = simulateScheduleAD(state0, model, schedule);

% Plot results in GUI
figure;
plotToolbar(G, states);
view(2);
axis tight;
colorbar;

