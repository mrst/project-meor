clear all
close all
tic

%grdecl = readGRDECL(fullfile(ROOTDIR,'examples','data','SAIGUP','SAIGUP.GRDECL'));
%usys = getUnitSystem('METRIC');
%grdecl = convertInputUnits(grdecl, usys);
%G = processGRDECL(grdecl);
nx = 40;
ny = 40;
nz = 4;
G = cartGrid([nx ny nz], [400 400 50]);
G = computeGeometry(G);
ijk = gridLogicalIndices(G);
%rock = grdecl2Rock(grdecl, G.cells.indexMap);
%is_pos = rock.perm(:,3) > 0;
%rock.perm(~is_pos,3) = 1e-6*min(rock.perm(is_pos,3));
rock.poro = ones(G.cells.num, 1).*.3;
rock.perm = ones(G.cells.num, 1).*100*milli*darcy;
nz = G.cartDims(3);
gravity on
%plotCellData(G, rock.poro);
%%
%blockIx = partitionUI(G,[4 4 1]);
 %  blockCol = rand(max(blockIx),1)*33;
  % plotCellData(G,mod(blockCol(blockIx),11),'EdgeAlpha',0.1);
   %axis tight off; view(-60,40);zoom(1.0); shading faceted
%%
% I = [3 20 3 25 3 30 5 29];
% J = [4 3 35 35 70 70 113 113];
% R = [1 1 1 1 1 1 1 1]*800;
% src = [];
% for i = 1:numel(I)
%     src_cell = find(ijk{1}==I(i) & ijk{2}==J(i) & ijk{3}==nz-3);
%     src = addSource(src, src_cell, R(i), 'sat', [1,0]);
% end
% I = [15 12 25 10 25];
% J = [20 60 50 95 95];
% for i = 1:numel(I)
%     src_cell = find(ijk{1}==I(i) & ijk{2}==J(i) & ijk{3}==3);
%     src = addSource(src, src_cell, -R(i), 'sat', [0,1]);
% end
src = addSource([], find(ijk{1}==2 & ijk{2}==2 & ijk{3}==nz-1),...
                800*meter^3/day, 'sat', [1 0]);
src = addSource(src, find(ijk{1}==nx-1 & ijk{2}==ny-1 & ijk{3}==2),...
                0, 'sat', [0 0]);
            
%src.m = zeros(length(src.cell),1);
src.meta = zeros(length(src.cell),1);
%src.n = zeros(length(src.cell),1);
src.m = [.2 0];
src.n = [1 0];


ooip = .84;
state0 = initResSol(G, 100*barsa, [1-ooip ooip]);
state0.m = zeros(G.cells.num, 1);
state0.n = zeros(G.cells.num, 1);
state0.meta = zeros(G.cells.num, 1);
state0.bio = zeros(G.cells.num, 1);
deck = readEclipseDeck('test.data');
deck = convertDeckUnits(deck);
fluid = initDeckADIFluid(deck);
%%
model = MEORaModel(G, rock, fluid);
model.stateplots = false;
model.biofilm = true;
model.biosurf = true;
model.yield_metabolite = .5;
model.yield_microbe = .5;
model.halfsat_microbe = 1;
model.halfsat_metabolite = 1;
model.growth_max_microbe = .1/day;
model.growth_max_metabolite = .1/day;
model.langmuir = [.001 .0017];

modelp = model;
modelp.biosurf = false;
modelp.biopoly = true;
modelow = TwoPhaseOilWaterModel(G, rock, fluid);

dt = 2*day;
nt = 500/(dt/day);
schedule.step.val = dt.*ones(nt,1);
schedule.step.control = ones(nt,1);
schedule.control(1).src = src;
schedule.control(1).W = [];
schedule.control(1).bc = [];

%%

[~, states_ow, schedulereport] = simulateScheduleAD(state0, modelow, schedule);
%[~, states_surf] = simulateScheduleAD(state0, model, schedule);
%[~, states_poly] = simulateScheduleAD(state0, modelp, schedule);
toc
%%

production = src.cell(end);
oilw = zeros(length(states_ow)+1, 1);
%oilw2 = oilw;
oils = oilw;
oilp = oils;
for i = 1:length(states_ow)
    oilw(i+1) = oilw(i) + sum(states_ow{i}.flux(production,2)).*dt;
    %oilw2(i+1) = oilw2(i) + ooip.*sum(model.operators.pv);
    %oils(i+1) = oils(i) + sum(states_surf{i}.flux(production,2)).*dt;    
    %oilp(i+1) = oilp(i) + sum(states_poly{i}.flux(production,2)).*dt;    
end
figure
plot(oilw./(sum(model.operators.pv).*ooip),'k:', 'LineWidth', 2)
%hold on
%plot(oils./(sum(model.operators.pv).*ooip), 'LineWidth', 2)
%plot(oilp./(sum(model.operators.pv).*ooip), 'r', 'LineWidth', 2)
legend('Water', 'Surfactant', 'Polymer','Location','Best')
disp(oilw(end)./(sum(model.operators.pv).*ooip))
%disp(oils(end)./(sum(model.operators.pv).*ooip))
%disp(oilp(end)./(sum(model.operators.pv).*ooip))
%disp((oils(end)-oilw(end))./oilw(end))
%disp((oilp(end)-oilw(end))./oilw(end))

%%
% figure
% plotToolbar(G,states_surf)
% colorbar
% axis tight
% figure
% plotToolbar(G,states_poly)
% colorbar
% axis tight
figure
plotToolbar(G,states_ow)
colorbar
axis tight

toc