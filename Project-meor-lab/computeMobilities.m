%%
%
% perlOneLineDescription(compute the mobilities)
%
% This function computes the oil, water and polymer mobilities given pressure,
% saturation and concentration values.
%

function [mobW, mobO, mobMic, mobNtr, mobMeta] = computeMobilities(p, sW, mic, ntr, meta, fluid, biopoly, biosurf)   

   change = zeros(length(meta),1);
    if biopoly && sum(meta)>0;
        % Threshold for effect
        meta_eff = meta;
        meta_eff(meta<1e-2) = 0;
        % power law from Lacerda too much?
         change = 1.4019.*meta_eff.^.1653;
        % parabolic 
        % change = (.414.*meta.^2 + 1.895.*meta).*(double(meta)~=0);
        % inx = change<1e-6;
        % change(~inx) = change(~inx) + .071;
        % change = change - fluid.muW(p);
         inx = change<1e-6;
         change(inx) = 0;
         
    end


   [krW, krO] = fluid.relPerm(sW);
    if biosurf
        % form is taken from Nielsen
        % constants are as well
        krO_max =  .8.*((1 - sW - .08)./(1 - .08 - .3)).^2;
        krW_max =  .5.*((sW - .3)./(1 - .08 - .3)).^2;
        meta_eff = meta.*10^3;
        partition = 1.*(sW.*fluid.rhoWS)./((1-sW).*fluid.rhoOS);
        meta_eff = meta_eff.*partition./(partition + 1);
        meta_eff(meta_eff<.01) = 0;
        meta_eff(meta_eff>1) = 1;
        krW = krW + (krW_max - krW).*meta_eff;
        krO = krO + (krO_max - krO).*meta_eff;
    end
   
   
   muW = fluid.muW(p);
   muW = muW + change.*1e-3;
   mobW = krW./muW;

   muO = fluid.BOxmuO(p)./fluid.BO(p);
   mobO = krO./muO;

   mobMic = mic.*mobW;
   mobNtr = ntr.*mobW;
   mobMeta = meta.*mobW;

end

