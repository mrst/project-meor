%%
%
% We set up a simple polymer simulator with a cartesian grid and physical boundary
% conditons such as given influx in input cells and given pressure, saturation and
% concentration on output faces.
% Add mrst modules which are used  
%

   mrstModule add ad-fi deckformat mrst-gui
   
   %%
   % Generate grid. We set up two cases: 1D or 2D. The changes in the remaining of the code
   % are minimal. They appear only in the file setupControls where the boundary
   % conditions are set.
   %

   sim_case = '1D'; % '1D' or '2D'
   param.simulation_case = sim_case;

   switch sim_case
     
     case '1D'

       nx = 400;
       ny = 1;
       nz = 1;
       
       xlength = 400;
       ylength = 100;
       zlength = 100;
     
     case '2D'

       nx = 100;
       ny = 100;
       nz = 1;
       
       xlength = 10;
       ylength = 10;
       zlength = 1;
   
   end

   %%
   % The function ¦cartGrid¦ generates a MRST unstructured grid. The function |computeGeometry|
   % computes geometrical properties such as cell volumes, faces areas...

   G = cartGrid([nx ny nz], [xlength, ylength, zlength]);
   G = computeGeometry(G);

   %%
   % Setup |rock| structure containing the rock properties.
   %

   rock.perm = repmat(100*milli*darcy, [G.cells.num, 1]);
   rock.poro = repmat(0.4, [G.cells.num, 1]);

   %%
   % Setup |fluid| structure containing the fluid and polymer properties form the |deck|
   % structure.
   %

   deck  = readEclipseDeck('MICROBE.DATA');
   deck  = convertDeckUnits(deck);
   fluid = initDeckADIFluid(deck);

   %%
   % We set off gravity. 
   %

   gravity off


   %%
   % Injection will be done in given cells.
   %

   bc.injection.rate = 800/day;
   bc.injection.s = 1;
   bc.injection.mic = .005;
   bc.injection.ntr = .01;
   
   
   %%
   % pressure, saturation and polymer concentration are imposed on given faces
   %

   bc.dirichlet.pressure = 100*barsa;
   bc.dirichlet.s = 0;
   bc.dirichlet.mic = 0;
   bc.dirichlet.ntr = 0;
   bc.dirichlet.met = 0;

   %%
   % The function perlAddLink(setupControls) set up the |bc| structure.
   %


   bc = setupControls(G, bc, sim_case);


   %%
   % The function perlAddLink(setupSystem) sets up the system. In particular, it defines
   % the discrete differential operators.
   %


   system = setupSystem(G, rock, bc);
   system.fluid = fluid;
   system.rock = rock;
   system.nonlinear.tol           = 1e-6;
   system.nonlinear.maxIterations = 30;
   system.nonlinear.relaxRelTol   = 0.2;
   system.cellwise                = 1:3;
   
   system.nonlinear.relaxInc = 1e-2;
   system.nonlinear.relaxMax = .5;
   
   %%
   % The transport part can be either solved explicitly or implicitly. From the
   % implementation point of view, it corresponds to one line of change in the code,
   %

   system.implicit_transport = true;
   

   %%
   % Set up the initial state with constant pressure, saturation and polymer concentration.
   %

   nc = G.cells.num;
   init_state.pressure = 1*atm*ones(nc,1);
   init_state.s        = ones(nc, 1)*[0.3, 0.7];
   init_state.mic     = zeros(G.cells.num, 1);
   init_state.ntr      = zeros(G.cells.num, 1);
   init_state.met      = zeros(G.cells.num, 1);
   init_state.biopoly = false;
   init_state.biosurf = true;


   %%
   % Compute and store the mobilities for the Dirichlet values
   %

   [bc.mobW, bc.mobO, bc.mobMic, bc.mobNtr, bc.mobMeta] = computeMobilities(bc.dirichlet.pressure, ...
                                                   bc.dirichlet.s       , ...
                                                   bc.dirichlet.mic       , ...
                                                   bc.dirichlet.ntr       , ...
                                                   bc.dirichlet.met       , ...
                                                   fluid, 0, 0);

   %%
   % Set up time stepping parameters. We use a fixed time step.
   %

   total_time = 400*day;
   dt         = 1*day;
   steps      = dt*ones(floor(total_time/dt), 1);
   t          = cumsum(steps);


   param.do_plot = true;
   
   if param.do_plot
      for i = 1 : 5
         figure(i)
      end
   end
   
   %% 
   % Time step iterations.
   %
   
   
   state0 = init_state;

   nsteps = numel(steps);
   states = cell(nsteps, 1);
   
   for tstep = 1 : nsteps

      dt = steps(tstep);
      
      %%
      % Call non-linear solver perlAddLink(solvefi) to compute next state
      %
      
      [state, conv, its] = solvefi(state0, dt, bc, system);

      if ~(conv)
         error('Convergence failed. Try smaller time steps.')
         return
      else
         fprintf('Step %d completed in %d Newton iterations\n', tstep, its);
      end

      states{tstep} = state;
      state0 = state;
      if param.do_plot
         plotState(state, G.cells.centroids(:, 1), param);
      end
   
   end