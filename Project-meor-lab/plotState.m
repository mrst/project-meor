function plotState(state, xvals, param)
    switch param.simulation_case
     case '1D'

       set(0, 'currentfigure', 1);
       plot(xvals, state.pressure);
       title(sprintf('Pressure'));
       
       set(0, 'currentfigure', 2);
       plot(xvals, state.s);
       title(sprintf('Saturation'));

       set(0, 'currentfigure', 3);
       plot(xvals, state.mic);
       title(sprintf('Microbe concentration'));

       set(0, 'currentfigure', 4);
       plot(xvals, state.ntr);
       title(sprintf('Nutrient concentration'));
       
       set(0, 'currentfigure', 5);
       plot(xvals, state.met);
       title(sprintf('Metabolite concentration'));
       
       drawnow;
     case {'2D'}
       % NOT IMPLEMENTED
       nComp = numel(state.C);
       for ic = 1:nComp
          set(0, 'currentfigure', ic);
          clf
          plotCellData(plot_params.G, state.C{ic});
          title(sprintf('Concentration %s', plot_params.legends{ic}));
       end
       set(0, 'currentfigure', nComp + 1);
       clf
       plotCellData(plot_params.G, state.sL);
       title('saturation');
       set(0, 'currentfigure', nComp + 2);
       clf
       plotCellData(plot_params.G, state.pressure);
       title('pressure');
       drawnow;
     otherwise
       error('simulation case not recognized.')
   end
end
