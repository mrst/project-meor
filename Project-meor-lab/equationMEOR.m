%%
%
%
% In this function, the modeling equations (in this case mass conservation of oil,
% water and polymer) are assembled. The function returns the
% residual vector (one value per cell) and the Jacobians with respect to the main
% variables (pressure, saturation and polymer concentration). 
%

function eqs = equationMEOR(state0, state, dt, bc, system, param)

   sys = system; % short-cut
   f = sys.fluid;
   poro = sys.rock.poro;

   
   %%
   % Initialize then main variables as Automatic Differentiation (AD) variables
   %
   p    = state.pressure;
   sW   = state.s(:,1);
   mic  = state.mic;
   ntr  = state.ntr;
   met = state.met;
   
   nc   = numel(p);
   [p, sW, mic, ntr, met] = initVariablesADI(p, sW, mic, ntr, met);
   

   %%
   % Initialize previous time step variables.
   %
   
   p0  = state0.pressure;
   sW0 = state0.s(:,1);
   mic0  = state0.mic;
   ntr0  = state0.ntr;
   meta0 = state0.met;
   
   %% 
   % Compute formation volume factors
   %
   
   bW     = f.bW(p);
   bO     = f.bO(p);
   bc_bW  = f.bW(bc.dirichlet.pressure); % boundary values 
   bc_bO  = f.bO(bc.dirichlet.pressure); % 

   %%
   % Computation of the pressure gradients
   %
   
   dpO = sys.grad(p, bc.dirichlet.pressure);
   dpW = dpO; % no capillary pressure

   %%
   % Computation of faces formation volume factor, given as average.
   %
   
   fbW = sys.averageFaceValues(bW, bc_bW);
   fbO = sys.averageFaceValues(bO, bc_bO);


   %% 
   % Computation of the mobilities, in the cells. The mobilities at the dirichlet boundary have
   % been already computed.
   %
   %
   % For an implicit solver, we compute the mobility using the current values for
   % pressure, saturation and polymer concentration. For an explicit solver, we use the
   % previous values. Note that this is the only modifications which are needed in the
   % code to switch between an implicit and explicit transport solver.
   %
   %
   
   if system.implicit_transport
      [mobW, mobO, mobMic, mobNtr, mobMeta] = computeMobilities(p, sW, mic, ntr, met, f, state.biopoly, state.biosurf);
   else
      [mobW, mobO, mobMic, mobNtr, mobMeta] = computeMobilities(p0, sW0, mic0, ntr0, meta0, f, state.biopoly, state.biosurf);
   end      
   
   
   %%
   % Computation of upstream phases mobilities.
   %
   
   wupc = (double(dpW)>=0);
   oupc = (double(dpO)>=0);
   fmobW = sys.upwindFaceValues(wupc, mobW, bc.mobW);
   fmobO = sys.upwindFaceValues(oupc, mobO, bc.mobO);
   fmobMic = sys.upwindFaceValues(wupc, mobMic, bc.mobMic);
   fmobNtr = sys.upwindFaceValues(wupc, mobNtr, bc.mobNtr);
   fmobMeta = sys.upwindFaceValues(wupc, mobMeta, bc.mobMeta);

   
   %%
   % Computation of fluxes
   %
   
   
   vW = fbW.*fmobW.*sys.T.*dpW;
   vO = fbO.*fmobO.*sys.T.*dpO;
   vMic = fbW.*fmobMic.*sys.T.*dpW;
   vNtr = fbW.*fmobNtr.*sys.T.*dpW;
   vMeta = fbW.*fmobMeta.*sys.T.*dpW;


   %%
   % Computation of accumulation terms for oil, water and polymer
   % The accumulation term for polymer includes adsorption
   % 
  
   
   accumO = (sys.pv/dt).*bO.*(1 - sW);
   accumW = (sys.pv/dt).*bW.*sW;
   accumMic = (sys.pv/dt).*bW.*sW.*mic;
   accumNtr = (sys.pv/dt).*bW.*sW.*ntr;
   accumMeta = (sys.pv/dt).*bW.*sW.*met;

   
   %%
   % Accumulation term for previous time step.
   %
   
   
   accumO0 = 1/dt*bO.*sys.pv.*(1 - sW0);
   accumW0 = 1/dt*bW.*sys.pv.*sW0;
   accumMic0 = 1/dt*bW.*sys.pv.*sW0.*mic0;
   accumNtr0 = 1/dt*bW.*sys.pv.*sW0.*ntr0;
   accumMeta0 = 1/dt*bW.*sys.pv.*sW0.*meta0;

   
   %%
   % Compute source term.
   %
   
   
   sInj = bc.injection.s;
   micInj = bc.injection.mic;
   ntrInj = bc.injection.ntr;
   influxO = zeros(nc, 1);
   influxW = zeros(nc, 1);
   influxMic = zeros(nc, 1);
   influxNtr = zeros(nc, 1);
   influxO(bc.injection.cells) = (1 - sInj)*bc.injection.rate;
   influxW(bc.injection.cells) = sInj*bc.injection.rate;
   influxMic(bc.injection.cells) = sInj*bc.injection.rate*micInj;
   influxNtr(bc.injection.cells) = sInj*bc.injection.rate*ntrInj;

   muMic = .002/day.*ntr./(.5 + ntr);
   Rb = muMic.*mic;
   growthMic = Rb.*bW.*sys.pv.*sW;
   muMeta = .002/day.*(ntr)./(.5 + ntr);
   Rm = muMeta.*mic;
   growthMeta = Rm.*bW.*sys.pv.*sW;
   decayNtr = (-Rb./.5-Rm./.5).*bW.*sys.pv.*sW;
   
   %%
   % assemble the equations as the sum of accumulation terms, flux terms and influxes.
   %
   
   eqs{1} = accumO - accumO0 + sys.div(vO) - influxO;
   eqs{2} = accumW - accumW0 + sys.div(vW) - influxW;
   eqs{3} = accumMic - accumMic0 + sys.div(vMic) - influxMic - growthMic;
   eqs{4} = accumNtr - accumNtr0 + sys.div(vNtr) - influxNtr - decayNtr;
   eqs{5} = accumMeta - accumMeta0 + sys.div(vMeta) - growthMeta;
end
