clear all

[nx, ny] = deal(32);
G = cartGrid([nx, ny], [500, 500]);
G = computeGeometry(G);
rock.perm = ones(G.cells.num,1)*100*milli*darcy;
rock.poro = ones(G.cells.num,1)*.3;
hT = computeTrans(G, rock);
gravity on
fluid = initCoreyFluid('mu' , [   1,  10]*centi*poise, ...
                  'rho', [1014, 859]*kilogram/meter^3, ...
                  'n'  , [   2,   2]                 , ...
                  'sr' , [ 0.2, 0.2]                 , ...
                  'kwm', [   1,   1]);
              
fluid.krW = fluid.relperm;              
              
state0 = initResSol(G, 100*barsa,[.2, .8]);
state0.m = zeros(G.cells.num,1);
state0.n = zeros(G.cells.num,1);

model = MEORModel(G, rock, fluid);

dt = 25*day;
nt = 50;
schedule.step.val = dt.*ones(nt,1);
schedule.step.control = ones(nt,1);
schedule.control(1).W = [];
schedule.control(1).bc = [];
schedule.control(1).src = [];


pv = sum(poreVolume(G,rock));
src = addSource([],1, pv);
src = addSource(src, G.cells.num, -pv);
schedule.control(1).src = src;

[~, states] = simulateScheduleAD(state0, model, schedule);

figure;
plotToolbar(G,states);
view([-10, 14]);
axis tight;
colorbar;
